const { createServer } = require('node:http');

const html = `<!DOCTYPE html>
<head>
<meta charset="utf-8"> 
</head>
<body>
`

async function requestListener(_req, res) {
  res.writeHead(200)
  res.end(html + `Le logiciel local n'est pas démarré car le logiciel distant est actif.<br><br>
Clique <a href='http://localhost:81/pull'>
    ici
</a> pour arrêter le logiciel distant, télécharger les données et démarrer le logiciel local.<br><br>
Clique <a href='https://edit.ppom.me/5eroue'>
    ici
</a> pour consulter le logiciel serveur.`)
}

const server = createServer(requestListener);
server.listen(process.env.PORT);
