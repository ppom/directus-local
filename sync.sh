#!/bin/sh

SSH="/usr/bin/ssh -i /data/ssh -o UserKnownHostsFile=/data/known_hosts -p 123 directus-5eroue@musi.ppom.me"

RESO="Impossible de se connecter au serveur. Es-tu sur Internet ?"
LOCO="Impossible de toucher à un fichier local. Mauvaise nouvelle."

die() {
  echo "$@"
  exit 1
}

pull() {
  if $SSH test -f nostart
  then
    if ! test -f nostart
    then
      die "Tu ne devrais pas pull. La dernière version des données est en local."
    fi
  fi
  $SSH doas systemctl stop directus-5eroue.service || die "$RESO"
  $SSH doas systemctl reset-failed directus-5eroue.service || die "$RESO"
  /usr/bin/rsync -avz -e "$SSH" :data.db :secrets /data || die "$RESO"
  $SSH touch nostart || die "$RESO"
  rm -f nostart || die "$LOCO"
}

push() {
  if ! $SSH test -f nostart
  then
    if test -f nostart
    then
      die "Tu ne devrais pas push. La dernière version des données est distante."
    fi
  fi
  /usr/bin/rsync -avz -e "$SSH" /data/data.db /data/secrets : || die "$RESO"
  $SSH rm -f nostart || die "$RESO"
  $SSH doas systemctl start directus-5eroue.service || die "$RESO"
  touch nostart || die "$LOCO"
}

case "$1" in
  pull) pull ;;
  push) push ;;
  *) die "Commande non reconnue (doit être push ou pull)" ;;
esac
