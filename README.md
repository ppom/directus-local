# Directus local

This script collection permits to run a copy of an online, on-server Directus, in a container on an offline laptop.

To prevent accidental data override, because of simultaneous edits on the two copies, the scripts ensure always one server is running.

This repository concerns the "local" part.

The "server" part is implemented on a NixOS server. You can find its relevant code [here](https://framagit.org/ppom/nixos/-/blob/main/modules/musi/edit.ppom.me/5eroue/default.nix) and [here](https://framagit.org/ppom/nixos/-/blob/main/modules/common/directus/default.nix).

It's a bit hacky and not general at all, but if it can give others ideas, that's cool!
