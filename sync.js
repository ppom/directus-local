const { createServer } = require('node:http');
const { spawn } = require('node:child_process');

const html = `<!DOCTYPE html>
<head>
<meta charset="utf-8"> 
</head>
<body>
`

function execute(command, args, cwd = '.') {
  return new Promise(function(resolve, _reject) {
    const subprocess = spawn(command, args, { cwd: cwd, encoding: 'utf8' });
    let out = "";
    let err = "";
    subprocess.stdout.on('data', data => out = out.concat(data));
    subprocess.stderr.on('data', data => err = err.concat('error: ' + data));
    subprocess.on('close', (code) => {
      resolve({ code, out, err });
    });
  });
}

async function requestListener(req, res) {
  const path = req.url.replaceAll('/', '');
  console.log("sync: " + path);
  if (path === 'push' || path === 'pull') {
    const { code, out, err } = await execute('sh', ['/data/sync.sh', path], '/data')
    console.log('sync: ' + path)
    console.log('sync: code: ' + code)
    console.log('sync: out: ' + out)
    console.log('sync: err: ' + err)
    if (code != 0) {
      res.writeHead(500)
      res.end(html + `${out}\n${err}`)
    } else {
      res.writeHead(200)
      if (path === 'pull') {
        res.end(html + `Téléchargement effectué : <br><br>Attends quelques secondes puis clique <a href='http://localhost'>
          ici
          </a> pour utiliser le logiciel local.`)
      } else {
        res.end(html + `
        Téléversement effectué : <br><br>Attends quelques secondes puis clique <a href='https://edit.ppom.me/5eroue'>
          ici
          </a> pour utiliser le logiciel distant.`)
      }
    }
  } else {
    res.writeHead(404);
    res.end(html + ` action inconnue. demande à /push ou /pull`);
  }
}

const server = createServer(requestListener);
server.listen(process.env.SYNC_PORT);
