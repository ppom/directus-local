#!/bin/sh

STATE=

is_nostart() {
  test -f /data/nostart
  return $?
}

start() {
  node /directus/cli.js bootstrap \
    && node /directus/cli.js start &
  STATE=start
}

nostart() {
  node /data/nostart.js &
  STATE=nostart
}

sync() {
  node /data/sync.js &
}

quit() {
    echo "entrypoint.sh: Quitting."
    # shellcheck disable=2046
    kill $(jobs -p)
    exit 0
}

trap quit INT

if test "$(id -nu)" = "root"
then
  if ! test -f /usr/bin/rsync
  then
    apk add rsync
  fi
  if ! test -f /usr/bin/ssh
  then
    apk add openssh-client
  fi
  su node -c "$0"
else

  sync

  if is_nostart
  then
    nostart
  else
    start
  fi

  while true
  do
    sleep 2
    if is_nostart
    then
      if test "$STATE" = start
      then
        # shellcheck disable=2046
        kill $(jobs -p)
        sync
        nostart
      fi
    else
      if test "$STATE" = nostart
      then
        # shellcheck disable=2046
        kill $(jobs -p)
        sync
        start
      fi
    fi
  done

fi
